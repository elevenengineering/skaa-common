#                  Copyright Eleven Engineering 2015-2016
# Thia Wyrod <wyrod@eleveneng.com>
# Sam Cristall <cristall@eleveneng.com>
# \file API_Consts.py
# \brief Contains system-wide data and functions for printing the data to C++.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

import enum, collections

# The following is based on Handles.itm information.
# In place of X macros or some other more limited monstrosity, we use Python
# cog to keep all of our register information in one place and conveniently
# generate our enum lists and such as required.

RegInfo = collections.namedtuple('RegInfo', [ 'index', 'len', 'read', 'write' ])

def MUD(name, x, idx_base):
  return (name+str(x), RegInfo(
    index=idx_base+x,
    len=8,
    read=True,
    write=True
    # Index x of the 64 byte-long multi-use data
    # char DataIndex0 DOXY(Index 0 of Multi Use Data)
    # char DataIndex1 DOXY(Index 1 of Multi Use Data)
    # char DataIndex2 DOXY(Index 2 of Multi Use Data)
    # char DataIndex3 DOXY(Index 3 of Multi Use Data)
    # char DataIndex4 DOXY(Index 4 of Multi Use Data)
    # char DataIndex5 DOXY(Index 5 of Multi Use Data)
    # char DataIndex6 DOXY(Index 6 of Multi Use Data)
    # char DataIndex7 DOXY(Index 7 of Multi Use Data)
  ))

def gTNx(x, raw_index):
  return ('gTN'+str(x), RegInfo(
    index=raw_index,
    len=8,
    read=True,
    write=True
    # Index x of an optional 32-character Unicode name for the
    # transmitter that is stored in non-volatile memory
    # char Char0 DOXY(Character 0+4*x MSB)
    # char Char1 DOXY(Character 0+4*x LSB)
    # char Char2 DOXY(Character 1+4*x MSB)
    # char Char3 DOXY(Character 1+4*x LSB)
    # char Char4 DOXY(Character 2+4*x MSB)
    # char Char5 DOXY(Character 2+4*x LSB)
    # char Char6 DOXY(Character 3+4*x MSB)
    # char Char7 DOXY(Character 3+4*x LSB)
  ))

def mGTx(x):
  return ('mGT'+str(x), RegInfo(
    index=66+x,
    len=8,
    read=True,
    write=False
    # The Hardware ID of the transmitter in green slot x
    # char TXIDindex0 DOXY(Index 0 of the transmitter's hardware ID)
    # char TXIDindex1 DOXY(Index 1 of the transmitter's hardware ID)
    # char TXIDindex2 DOXY(Index 2 of the transmitter's hardware ID)
    # char TXIDindex3 DOXY(Index 3 of the transmitter's hardware ID)
    # char TXIDindex4 DOXY(Index 4 of the transmitter's hardware ID)
    # char TXIDindex5 DOXY(Index 5 of the transmitter's hardware ID)
    # char TXIDindex6 DOXY(Index 6 of the transmitter's hardware ID)
    # char TXIDindex7 DOXY(Index 7 of the transmitter's hardware ID)
  ))

def mCNx(x, raw_index):
  return ('mCN'+str(x), RegInfo(
    index=raw_index,
    len=8,
    read=True,
    write=True
    # Index x of an optional 32-character Unicode name for the
    # bay that is stored in non-volatile memory
    # char Char0 DOXY(Character 0+4*x MSB)
    # char Char1 DOXY(Character 0+4*x LSB)
    # char Char2 DOXY(Character 1+4*x MSB)
    # char Char3 DOXY(Character 1+4*x LSB)
    # char Char4 DOXY(Character 2+4*x MSB)
    # char Char5 DOXY(Character 2+4*x LSB)
    # char Char6 DOXY(Character 3+4*x MSB)
    # char Char7 DOXY(Character 3+4*x LSB)
  ))

def nRNx(x):
  return ('nRN'+str(x), RegInfo(
    index=96+x,
    len=8,
    read=True,
    write=True
    # Index x of an optional 16-character Unicode name for the
    # receiver that is stored in non-volatile memory
    # char Char0 DOXY(Character 0+4*x MSB)
    # char Char1 DOXY(Character 0+4*x LSB)
    # char Char2 DOXY(Character 1+4*x MSB)
    # char Char3 DOXY(Character 1+4*x LSB)
    # char Char4 DOXY(Character 2+4*x MSB)
    # char Char5 DOXY(Character 2+4*x LSB)
    # char Char6 DOXY(Character 3+4*x MSB)
    # char Char7 DOXY(Character 3+4*x LSB)
  ))

def RegDataTableToStr(table, data_call = lambda v: v):
  s = [ '{0:s} = {1:d}'.format(k, data_call(v)) for k,v in table.items() ]
  return ',\n'.join(s)

def EnumTableSetStr(class_name, type_name, table, data_call = lambda v: v):
  s = [ 'enum class %s : %s {' %(class_name, type_name) ]
  s.append(RegDataTableToStr(table, data_call))
  s.append('};')
  return '\n'.join(s)


kAllRegs = collections.OrderedDict([
  ('hCMD', RegInfo(index=0, len=1, read=False, write=True)),
  #Pushes a command to the Host Device
  #char command DOXY(A command sent to the host)

  ('hPLS', RegInfo(index=1, len=1, read=True, write=False)),
  #Gets the current play status of the Host Device
  #char playStatus DOXY(The current play status)

  ('hVOL', RegInfo(index=2, len=2, read=True, write=True)),
  #Gets or setsthe current Host Volume
  #char leftVolume DOXY(Left volume to push)
  #char rightVolume DOXY(Right volume to push)

  ('hEQS', RegInfo(index=3, len=1, read=True, write=False)),
  #Gets the current SKAA Indexed EQ Setting from the Host Device
  #char EQsetting DOXY(The current EQ Setting (From 1 to 11))

  ('hTRP', RegInfo(index=4, len=2, read=True, write=False)),
  #Gets the current track info from the Host Device
  #char trackNumber DOXY(The number of the currently playing track)
  #char trackTotal DOXY(The number of Tracks in the current playlist)

  ('hBAT', RegInfo(index=5, len=1, read=True, write=False)),
  #Gets the current battery info from the Host Device
  #char batteryLevel DOXY(The current battery level with 16 being max and 0 being minimum)

  ('hDAT', RegInfo(index=6, len=6, read=True, write=False)),
  #Gets the current date and time of the Host Device
  #char date1 DOXY(Date data (FIXME))
  #char date2 DOXY(Date data (FIXME))
  #char date3 DOXY(Date data (FIXME))
  #char date4 DOXY(Date data (FIXME))
  #char date5 DOXY(Date data (FIXME))
  #char date6 DOXY(Date data (FIXME))

  ('hTTM', RegInfo(index=7, len=2, read=True, write=False)),
  #Gets the current track time from the Host Device
  #char minutesElapsed DOXY(The number of minutes elapsed in the track)
  #char secondsElapsed DOXY(The number of seconds elapsed in the track)

  ('hSSL', RegInfo(index=8, len=1, read=True, write=False)),
  #Gets the current standby or sleep state of the Host Device
  #char hostState DOXY(The current state of the Host Device)

  ('gAUP', RegInfo(index=9, len=1, read=True, write=False)),
  #Gets the current Audio status
  #char audioStatus DOXY(The audio status with 1 indicating audio present and 0 indicating no audio present)

  ('gCFG', RegInfo(index=10, len=1, read=True, write=True)),
  #Gets or sets the current configuration of the transmitter
  #char config DOXY(The configuration of the transmitter)

  ('gRMP', RegInfo(index=11, len=2, read=True, write=False)),
  #Gets the current Node Map of the system
  #char mapIndex0 DOXY(Bay1 with R3/R2/R1/R0 status and Bay0 with R3/R2/R1/R0 status)
  #char mapIndex1 DOXY(Bay3 with R3/R2/R1/R0 status and Bay2 with R3/R2/R1/R0 status)

  ('gIRQ', RegInfo(index=12, len=8, read=True, write=True)),
  #Gets or sets the currently configured register set that will to IRQ a Host Application
  #char IRQindex0 DOXY(Registration index 0)
  #char IRQindex1 DOXY(Registration index 1)
  #char IRQindex2 DOXY(Registration index 2)
  #char IRQindex3 DOXY(Registration index 3)
  #char IRQindex4 DOXY(Registration index 4)
  #char IRQindex5 DOXY(Registration index 5)
  #char IRQindex6 DOXY(Registration index 6)
  #char IRQindex7 DOXY(Registration index 7)

  ('gIRE', RegInfo(index=13, len=1, read=True, write=True)),
  #An 8-bit enable vector for the 8 handles contained in gIRQ.
  #Bit 0 = enable index 0 .. Bit 7 = enable index 7
  #char IREvector DOXY(IRE vector)

  ('gTXM', RegInfo(index=14, len=1, read=True, write=False)),
  #Gets the Transmitters model number
  #char model DOXY(The model number)

  ('gTHI', RegInfo(index=15, len=8, read=True, write=False)),
  #Gets the Transmitters unique hardware ID
  #char byte0 DOXY(Byte 0 of the Hardware ID)
  #char byte1 DOXY(Byte 1 of the Hardware ID)
  #char byte2 DOXY(Byte 2 of the Hardware ID)
  #char byte3 DOXY(Byte 3 of the Hardware ID)
  #char byte4 DOXY(Byte 4 of the Hardware ID)
  #char byte5 DOXY(Byte 5 of the Hardware ID)
  #char byte6 DOXY(Byte 6 of the Hardware ID)
  #char byte7 DOXY(Byte 7 of the Hardware ID)

  ('gTHV', RegInfo(index=16, len=1, read=True, write=False)),
  #Gets the Transmitters hardware version number
  #char version DOXY(The hardware version number)

  ('gTSV', RegInfo(index=17, len=8, read=True, write=False)),
  #Gets the Transmitters software version number
  #char version DOXY(The software version number)

  gTNx(0, 18),
  gTNx(1, 19),
  gTNx(2, 20),
  gTNx(3, 21),
  MUD('hMD', 0, 22),
  MUD('hMD', 1, 22),
  MUD('hMD', 2, 22),
  MUD('hMD', 3, 22),
  MUD('hMD', 4, 22),
  MUD('hMD', 5, 22),
  MUD('hMD', 6, 22),
  MUD('hMD', 7, 22),
  ('gFBD', RegInfo(index=30, len=1, read=False, write=True)),
  #This is an API call to indicate a potentially failed bond due to overflow and desync -- setting it will initiate a refresh of data parameters
  #char Slot DOXY(The slot of the receiver that failed bond up)

  ('hREQ', RegInfo(index=31, len=2, read=False, write=True)),
  #This is an API call for the Host to Request an API from the Device
  #char API DOXY(The SKAA API being requested from the Host)
  #char DEST DOXY(The Destination associated with API requested from the Host)

  ('hREA', RegInfo(index=32, len=1, read=False, write=True)),
  #This is an API call for the Device to Request an API from the Host
  #char API DOXY(The SKAA API request to pass to the Host)

  ('hSYN', RegInfo(index=33, len=1, read=False, write=True)),
  #This is an API for the Host to request a re-sync of current information (This could vary from Tx to Tx)
  #char dummy DOXY(Simply writing to hSYN causes a sync so this byte is not used)

  ('hDEB', RegInfo(index=34, len=8, read=True, write=False)),
  #This is a Host Specific Debug Register
  #char Debug1 DOXY(1st Debug Char)
  #char Debug2 DOXY(2nd Debug Char)
  #char Debug3 DOXY(3rd Debug Char)
  #char Debug4 DOXY(4th Debug Char)
  #char Debug5 DOXY(5th Debug Char)
  #char Debug6 DOXY(6th Debug Char)
  #char Debug7 DOXY(7th Debug Char)
  #char Debug8 DOXY(8th Debug Char)

  ('gPWR', RegInfo(index=35, len=1, read=False, write=True)),
  #Does power stuff

  ('gVOF', RegInfo(index=36, len=1, read=True, write=False)),
  #Virtual Off Status

  ('hMUT', RegInfo(index=37, len=1, read=True, write=True)),
  #Gets or sets the current global host mute as a boolean value
  #char mute DOXY(The current mute state)

  gTNx(4, 38),
  gTNx(5, 39),
  gTNx(6, 40),
  gTNx(7, 41),
  ('gDLY', RegInfo(index=42, len=2, read=True, write=True)),
  #Value which represents the audio delay (in ms) before sending audio to the
  #D-Amp in the product driver. Byte0 is LSB.

  ('mGLP', RegInfo(index=64, len=1, read=True, write=False)),
  #Gets the current Green List Pointer
  #char glp DOXY(The position of the green list pointer from 0-9)

  ('mGLS', RegInfo(index=65, len=1, read=True, write=False)),
  #Gets the current Green List Size
  #char gls DOXY(The size of the green list from 0-9)

  mGTx(0),
  mGTx(1),
  mGTx(2),
  mGTx(3),
  mGTx(4),
  mGTx(5),
  mGTx(6),
  mGTx(7),
  mGTx(8),
  mGTx(9),
  ('mSKB', RegInfo(index=76, len=1, read=False, write=True)),
  #Pushes a SKAA Button Command to the Bay Master
  #char command DOXY(The SKAA Button Command to send)

  ('mCMD', RegInfo(index=77, len=0, read=False, write=False)),
  #[DEPRECATED] Pushes a Contextless SKAA Command to the Bay Master
  #char command DOXY(The SKAA Command to send)

  mCNx(0, 78),
  mCNx(1, 79),
  mCNx(2, 80),
  mCNx(3, 81),
  ('nVOL', RegInfo(index=82, len=3, read=True, write=False)),
  #Gets the current calculated volume for the receiver
  #char leftVolume DOXY(The left channel volume)
  #char rightVolume DOXY(The right channel volume)
  #char monoVolume DOXY(The averaged mono volume)

  ('nVTR', RegInfo(index=83, len=1, read=True, write=True)),
  #Gets or sets the current volume trim for the receiver as an 8-bit fraction from 0 to 1
  #char trim DOXY(The volume trim)

  ('nRAU', RegInfo(index=84, len=1, read=True, write=True)),
  #Gets the audio channels that the receiver is configured to play
  #char channels DOXY(0 = Stereo / 1 = Left / 2 = Right / 3 = Mono / 4 = Swap / 5 = Differential Left, 6 = Differential Right / All else are invalid)

  ('nRAL', RegInfo(index=85, len=1, read=True, write=False)),
  #A bit map of the valid nRAU settings, where a set position indicates valid nRAU setting
  #char legal_ch_bitmap DOXY(each bit is 1 << setting / where setting is the valid value from nRAU)

  ('nRHI', RegInfo(index=86, len=8, read=True, write=False)),
  #Gets the hardware ID of the receiver
  #char HIDindex0 DOXY(Byte 0 of the HID)
  #char HIDindex1 DOXY(Byte 1 of the HID)
  #char HIDindex2 DOXY(Byte 2 of the HID)
  #char HIDindex3 DOXY(Byte 3 of the HID)
  #char HIDindex4 DOXY(Byte 4 of the HID)
  #char HIDindex5 DOXY(Byte 5 of the HID)
  #char HIDindex6 DOXY(Byte 6 of the HID)
  #char HIDindex7 DOXY(Byte 7 of the HID)

  ('nRXD', RegInfo(index=87, len=1, read=True, write=False)),
  #The receiver designation with 0 meaning Bay master and 1/2/3 indicating slave and slot
  #char designator DOXY(0 = Master 1/2/3 = Slave and Slot#)

  ('nPDS', RegInfo(index=88, len=1, read=True, write=False)),
  #The current configuration of the Parameter Table Select Pins
  #char pds DOXY(0/1/2/3 indicating the current configuration of the select pins)

  ('nSKU', RegInfo(index=89, len=4, read=True, write=False)),
  #The 4-byte receiver SKUID which is unique to the Receiver
  #char SKUindex0 DOXY(Byte 0 of the receiver SKUID)
  #char SKUindex1 DOXY(Byte 1 of the receiver SKUID)
  #char SKUindex2 DOXY(Byte 2 of the receiver SKUID)
  #char SKUindex3 DOXY(Byte 3 of the receiver SKUID)

  ('nIRQ', RegInfo(index=90, len=8, read=True, write=True)),
  #Contains 8 configurable Handle indices that will result in an automatic interrupt when changed within the SKAA system
  #char IRQindex0 DOXY(Registration index 0)
  #char IRQindex1 DOXY(Registration index 1)
  #char IRQindex2 DOXY(Registration index 2)
  #char IRQindex3 DOXY(Registration index 3)
  #char IRQindex4 DOXY(Registration index 4)
  #char IRQindex5 DOXY(Registration index 5)
  #char IRQindex6 DOXY(Registration index 6)
  #char IRQindex7 DOXY(Registration index 7)

  ('nIRE', RegInfo(index=91, len=1, read=True, write=True)),
  #An 8-bit enable vector for the 8 handles contained in nIRQ.  Bit 0 = enable index 0 .. Bit 7 = enable index 7
  #char IREvector DOXY(IRE vector)

  ('nSTE', RegInfo(index=92, len=1, read=True, write=False)),
  #Gets the receivers current state including Power Conserve / Sleep / Standby
  #char state DOXY(Bit 0 = Power Conserve / Bit 1 = Sleep / Bit 2 = Standby / Bit 3 = Mute / Bit 4-5 = Bond State)

  ('nRHV', RegInfo(index=93, len=1, read=True, write=False)),
  #Gets the hardware version of the
  #char version DOXY(The SiP version number)

  ('nRSV', RegInfo(index=94, len=8, read=True, write=False)),
  #Gets the current software version of the SKAA OS
  #char version DOXY(The SKAA OS version number)

  ('nPST', RegInfo(index=95, len=1, read=True, write=False)),
  #Indicates the current power state of the receiver.
  #Bit 0 = Power Conserve / Bit 1 = Sleep / Bit 2 = Standby

  nRNx(0),
  nRNx(1),
  nRNx(2),
  nRNx(3),
  ('nAVL', RegInfo(index=100, len=1, read=True, write=True)),
  #Gets or sets the Aux In-Only volume
  #int volume DOXY(The Aux In-Only volume)

  ('nAMT', RegInfo(index=101, len=1, read=True, write=True)),
  #Gets or sets the Aux In-Only mute state
  #int mute DOXY(The Aux In-Only mute state)

  ('nREQ', RegInfo(index=102, len=1, read=False, write=True)),
  #A special request handle for reading handles.
  #Writing to this handle requests a handle from the system.
  #When the IRQ pin goes high, reading this register will return a requested
  #handle, but not necessarily in the same order.
  #char handle DOXY(The handle to request)

  ('nBAN', RegInfo(index=103, len=1, read=True, write=True)),
  #Gets the bay number of this receiver (Note that you can write to this but it will do nothing)
  #char bay DOXY(The bay number of this receiver)

  mCNx(4, 104),
  mCNx(5, 105),
  mCNx(6, 106),
  mCNx(7, 107),
  ('mNCR', RegInfo(index=108, len=1, read=True, write=False)),
  #Number of receivers registered to this master receiver in a cluster.

  ('bVTR', RegInfo(index=128, len=1, read=True, write=True)),
  #Gets or sets the current Bay-wide trim as an 8-bit fraction from 0 to 1
  #char trim DOXY(The volume trim)

  ('bMUT', RegInfo(index=129, len=1, read=True, write=True)),
  #Gets or sets the current Bay-wide mute as a boolean value
  #char mute DOXY(The current mute state)

  ('bCIH', RegInfo(index=130, len=5, read=True, write=False)),
  #Upper 5 bytes of ClusterID

  ('bCIL', RegInfo(index=131, len=5, read=True, write=False)),
  #Lower 5 bytes of ClusterID

  ('bBAN', RegInfo(index=132, len=1, read=True, write=False)),
  #Gets the number of the bay that the receiver is currently docked in
  #char number DOXY(The bay index that the receiver is docked in)

  ('bBAS', RegInfo(index=133, len=4, read=True, write=False)),
  #Gets the lowest order SKU byte of every node in the currently docked bay where 0xFF is invalid
  #char SKU0 DOXY(The lowest order SKU byte of node 0 which is also the master)
  #char SKU1 DOXY(The lowest order SKU byte of node 1)
  #char SKU2 DOXY(The lowest order SKU byte of node 2)
  #char SKU3 DOXY(The lowest order SKU byte of node 3)

  ('bBSF', RegInfo(index=134, len=3, read=True, write=False)),
  #Gets the top 3 SKU bytes which are common across the currently docked bay
  #char CommonSKU0 DOXY(Byte 0 of the common top 3 SKU bytes)
  #char CommonSKU1 DOXY(Byte 1 of the common top 3 SKU bytes)
  #char CommonSKU2 DOXY(Byte 2 of the common top 3 SKU bytes)

  ('bHVE', RegInfo(index=135, len=1, read=True, write=True)),
  #If 0, ignore hVOL when computing nVOL (use 0xFF in place of hVOL).
  #char Enable DOXY(Host Volume Enable)

  ('bUSR', RegInfo(index=136, len=8, read=True, write=True)),
  #Bay-wide, arbitrary data values strictly for client use.

  MUD('bMD', 2, 135),
  MUD('bMD', 3, 135),
  ('bADD', RegInfo(index=139, len=8, read=True, write=True)),
  #Application Data Downlink for Host App to communicate to Bay
  #char DataIndex0 DOXY(Index 0 of Multi Use Data)
  #char DataIndex1 DOXY(Index 1 of Multi Use Data)
  #char DataIndex2 DOXY(Index 2 of Multi Use Data)
  #char DataIndex3 DOXY(Index 3 of Multi Use Data)
  #char DataIndex4 DOXY(Index 4 of Multi Use Data)
  #char DataIndex5 DOXY(Index 5 of Multi Use Data)
  #char DataIndex6 DOXY(Index 6 of Multi Use Data)
  #char DataIndex7 DOXY(Index 7 of Multi Use Data)

  ('bADU', RegInfo(index=140, len=8, read=True, write=True)),
  #Application Data Uplink for Bay to communicate to Host App
  #char DataIndex0 DOXY(Index 0 of Multi Use Data)
  #char DataIndex1 DOXY(Index 1 of Multi Use Data)
  #char DataIndex2 DOXY(Index 2 of Multi Use Data)
  #char DataIndex3 DOXY(Index 3 of Multi Use Data)
  #char DataIndex4 DOXY(Index 4 of Multi Use Data)
  #char DataIndex5 DOXY(Index 5 of Multi Use Data)
  #char DataIndex6 DOXY(Index 6 of Multi Use Data)
  #char DataIndex7 DOXY(Index 7 of Multi Use Data)

  MUD('bMD', 6, 135),
  MUD('bMD', 7, 135),
  ('bBTP', RegInfo(index=143, len=1, read=True, write=True)),
  #Gets the current bond type with 0 being Amber / 1 being Green / 2 being Swing
  #char Type DOXY(Bond type)

  ('WirelessBondUpdate', RegInfo(index=256, len=1, read=False, write=False)),
  #Indicates the current bond to a transmitter, where 1 is bonded and 0 is unbonded

  ('AuxInState', RegInfo(index=257, len=1, read=False, write=False)),
  #Indicates whether the device is in AuxIn mode, where 1 is true and 0 is false

  ('TxAddFail', RegInfo(index=258, len=0, read=False, write=False)),
  #Dummy register to signal that the addition of a transmitter to the green
  #list failed

  ('OutgoingGet', RegInfo(index=259, len=8, read=False, write=False)),
  #Packed buffer of handles the receiver wishes to get from the transmitter
  #-- maps directly to transmitted packed
  #char handle1 DOXY(Handle to request)
  #char reserved DOXY(Reserved -- set to zero)
  #char handle2 DOXY(Handle to request)
  #char reserved DOXY(Reserved -- set to zero)
  #char handle3 DOXY(Handle to request)
  #char reserved DOXY(Reserved -- set to zero)
  #char handle4 DOXY(Handle to request)
  #char reserved DOXY(Reserved -- set to zero)

  ('IncomingGet', RegInfo(index=260, len=8, read=False, write=False)),
  #Packed buffer of handles a transmitter wishes to get from a receiver
  #char handle1 DOXY(Requested handle)
  #char reserved DOXY(Reserved)
  #char handle2 DOXY(Requested handle)
  #char reserved DOXY(Reserved)
  #char handle3 DOXY(Requested handle)
  #char reserved DOXY(Reserved)
  #char handle4 DOXY(Requested handle)
  #char reserved DOXY(Reserved)

  ('OutgoingSet', RegInfo(index=261, len=1, read=False, write=False)),
  #Handle of local API that should be pushed up to the transmitter
  #char handle DOXY(Handle to push to transmitter)

  ('TxAddSuccess', RegInfo(index=262, len=0, read=False, write=False)),
  #Dummy register to signal that the addition of a transmitter to the green
  #list succeeded

  ('TxDelete', RegInfo(index=263, len=0, read=False, write=False)),
  #Dummy register to signal that the deletion of a transmitter from the green
  #list succeeded

  ('VolUp', RegInfo(index=264, len=0, read=False, write=False)),
  #Dummy register to signal the hardware volume up button being pushed.

  ('VolDown', RegInfo(index=265, len=0, read=False, write=False)),
  #Dummy register to signal the hardware volume down button being pushed.

  ('SaveGLP', RegInfo(index=266, len=0, read=False, write=False)),
  #Dummy register to signal that the GLP at the time of button push should be
  #saved.

  ('BondState', RegInfo(index=267, len=1, read=False, write=False)),
  #Indicates the receiver's current bond state

  ('PowerConserve', RegInfo(index=268, len=1, read=False, write=False)),
  #Indicates whether the receiver is in power conserve mode or not

  ('UpdateGLP', RegInfo(index=269, len=0, read=False, write=False)),
  #Dummy register to signal that the GLP should be updated to match the
  #currently bonded transmitter.

  ('ForcePowerDown', RegInfo(index=270, len=0, read=False, write=False)),
  #Dummy register to signal that we should enter power down, as if Power Down Prep
  #were asserted

  ('Varactor', RegInfo(index=271, len=2, read=False, write=False)),
  #BBU register to pass varactor values.

  ('BootNotify', RegInfo(index=272, len=0, read=False, write=False)),
  #BBU register to state that the ginseng has booted up

  ('NadjaDACProgrammingComplete', RegInfo(index=273, len=0, read=False, write=False)),
  #Nadja DAC Programming completion notification

  ('NadjaProjectDefinedData', RegInfo(index=274, len=8, read=False, write=False)),
  #:adja custom data defined by a particular product

  ('NadjaShutdown', RegInfo(index=275, len=0, read=False, write=False)),
  #Nadja shutdown notification

  ('StandbyButtonActivity', RegInfo(index=276, len=0, read=False, write=False)),
  #Notification to wake up from low power state due to user input
])

kAllFirmwareRegTables = collections.OrderedDict([
  ('Sys', [ v for v in kAllRegs.values() if v.index < 64 ] ),
  ('Node', [ v for v in kAllRegs.values() if v.index >= 64 and v.index < 128 ] ),
  ('Bay', [ v for v in kAllRegs.values() if v.index >= 128 and v.index < 256 ] ),
  ('Internal', [ v for v in kAllRegs.values() if v.index >= 256 ] ),
])

kAllSoftwareRegTables = collections.OrderedDict([
  ('Sys', [ v for v in kAllRegs.values() if v.index < 64 ] ),
  ('Node', [ v for v in kAllRegs.values() if v.index >= 82 and v.index < 104 ] ),
  ('Master', [ v for v in kAllRegs.values()
    if (v.index >= 64 and v.index < 82) or (v.index >= 104 and v.index < 109) ]
  ),
  ('Bay', [ v for v in kAllRegs.values()
    if (v.index >= 128 and v.index < 256) ]
  ),
  ('Internal', [ v for v in kAllRegs.values() if v.index >= 256 ] ),
])

kExternalRegs = collections.OrderedDict([
  (k,v) for k,v in kAllRegs.items() if v.index < 256
])

kExternalIndices = { k: v.index for k,v in kExternalRegs.items() }
kExternalIndices['Invalid'] = 255
RegIndex = enum.Enum('RegIndex', kExternalIndices)

kPackedDataSizeInBytes = 8
kRegConsts = collections.OrderedDict([
  ('kMaxDataLength', max(datum.len for datum in kAllRegs.values())),
  ('kGreenListLen', 10),
  ('kRxNameLen', 8),
  ('kBaseUnitBits', 16),
  ('kInvalidHandle', 0xFF),
  ('kPackedDataSizeInWords', int(kPackedDataSizeInBytes/2))
])
