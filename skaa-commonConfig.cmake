#=============================================================================
# Copyright 2016 Eleven Engineering Inc.
#
# Distributed under the OSI-approved BSD License.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#=============================================================================
# Provides the following variables:
# skaa-common_INCLUDE_DIRS - Directories containing all of the headers
# skaa-common_PY_DIRS      - Directories containing all of the py scripts
# skaa-common_FOUND        - Boolean indicating whether skaa-common was found
#=============================================================================
# Reads the following variables (if provided):
# skaa-common_DIR    - Preferred directory to designate as skaa-common
#=============================================================================

function(dirs_from_ext ext dirs)
  file(GLOB_RECURSE files FOLLOW_SYMLINKS ${skaa-common_BASE_DIR}/*.${ext})
  foreach (f ${files})
    get_filename_component(dir ${f} DIRECTORY)
    list(APPEND local_list ${dir})
  endforeach()
  if (local_list)
    list(REMOVE_DUPLICATES local_list)
  endif()
  set(${dirs} ${local_list} PARENT_SCOPE)
endfunction()

find_path(skaa-common_BASE_DIR py/API_Consts.py
  PATH_SUFFIXES skaa-common skaa-common/include/skaa-common
)
mark_as_advanced(skaa-common_BASE_DIR)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(skaa-common
  REQUIRED_VARS skaa-common_BASE_DIR
)

dirs_from_ext("h" skaa-common_INCLUDE_DIRS)
dirs_from_ext("py" skaa-common_PY_DIRS)
